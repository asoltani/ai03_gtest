#include <gtest/gtest.h>
#include <random>

#include "../Barrage/Barrage.hpp"

// prend environ 5min (Guillame si jamais t'as trop froid lance ça sur ton ordi rose x))
TEST(test_long_barrage, utilisation_realiste) {
    Barrage b(200, 1000, 3.75, 100, 0);

    // Générer des vecteurs prix et ruissellement aléatoires
    random_device rnd_device;
    mt19937 mersenne_engine {rnd_device()};
    normal_distribution<float> normal;
    vector<float> prix(10000);
    vector<int> ruissellement(10000);
    generate(prix.begin(), prix.end(), [&normal, &mersenne_engine](){
        return max(0.0f, normal(mersenne_engine) * 20 + 50);
    });
    generate(ruissellement.begin(), ruissellement.end(), [&normal, &mersenne_engine](){
        float r = normal(mersenne_engine) * 10;
        return max(0, static_cast<int>(r));
    });

    b.setPrixMarcheElec(prix).setRuissellement(ruissellement);
    b.simuler();

    auto& valeurs = b.getGrilleBellmanValeur();
    auto policy = b.optimiser();

    int v_max = policy.back();
    for (size_t i=0; i<valeurs.rows(); i++)
    {
        if (valeurs(i, valeurs.cols()-1) > valeurs(v_max, valeurs.cols()-1))
        {
            ostringstream oss;
            oss << "La politique de gestion n'est pas optimale" << endl;
            for (auto& p : policy)
                oss << p << ' ';
            oss << endl;
            oss << endl << valeurs << endl;

            FAIL() << oss.str();
        }
    }
}
