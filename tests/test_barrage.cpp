#include <gtest/gtest.h>

#include "../Barrage/Barrage.hpp"


TEST(test_barrage, setup_barrage) {
    Barrage b(2, 3, 1, 1, 0);
    vector<float> prix{1, 2, 3};
    vector<int> ruissellement{1, 2, 3, 4};

    ASSERT_THROW(b.setRuissellement(ruissellement).setPrixMarcheElec(prix), invalid_argument);
    ASSERT_THROW(b.setPrixMarcheElec(prix).setRuissellement(ruissellement), invalid_argument);

    ruissellement.pop_back();

    Barrage b1(2, 3, 1, 1, 0);
    b1.setPrixMarcheElec(prix).setRuissellement(ruissellement);

    ASSERT_THROW(Barrage b2(0, 0, -1, 0, 0), invalid_argument);
    ASSERT_THROW(Barrage b2(0, 0, 0, 0, 1), invalid_argument);
    ASSERT_THROW(Barrage b2(1, 0, 0, 0, 0), invalid_argument);
}

TEST(test_barrage, initialisation_matrices) {
    int v_max = 3;
    Barrage b(2, v_max, 1, 1, 0);
    vector<float> prix{1, 2, 3};

    ASSERT_THROW(b.getGrilleBellmanValeur(), runtime_error);
    ASSERT_THROW(b.getGrilleBellmanParent(), runtime_error);

    b.setPrixMarcheElec(prix);
    auto expected_grille_valeur = Barrage::GrilleBellmanValue::Constant(v_max+1, prix.size()+1, -1.0f);
    auto& grille_valeur = b.getGrilleBellmanValeur();
    EXPECT_EQ(expected_grille_valeur, grille_valeur) << "grille de bellman des valeurs:\n" << grille_valeur << '\n';

    Barrage b1(2, v_max, 1, 1, 0);
    b1.setPrixMarcheElec(prix);
    auto expected_grille_parent = Barrage::GrilleBellmanParent::Constant(v_max+1, prix.size()+1, -1);
    auto& grille_parent = b.getGrilleBellmanParent();
    EXPECT_EQ(expected_grille_parent, grille_parent) << "grille de bellman des parents:\n" << grille_parent << '\n';
}

TEST(test_barrage, simulation1) {
    int v_max = 3;
    Barrage b(2, v_max, 2, 1, 0);
    vector<float> prix = {2, 3, 1};
    vector<int> ruissellement = {1, 2, 0};
    b.setPrixMarcheElec(prix).setRuissellement(ruissellement);

    b.simuler();
    auto& grille_valeur = b.getGrilleBellmanValeur();
    auto& grille_parent = b.getGrilleBellmanParent();

    Barrage::GrilleBellmanValue expected_grille_valeur(v_max+1, prix.size()+1);
    expected_grille_valeur <<           -1.0f,  -1.0f,  -1.0f,  0.0f,
                                        -1.0f,  -1.0f,  -1.0f,  4.0f,
                                        0.0f,   4.0f,   -1.0f,  20.0f,
                                        -1.0f,  0.0f,   10.0f,  22.0f;
    Barrage::GrilleBellmanParent expected_grille_parent(v_max+1, prix.size()+1);
    expected_grille_parent <<           -1,  -1,  -1,  -1,
                                        -1,  -1,  -1,  -1,
                                        -1,  2,   -1,  3,
                                        -1,  2,   2,   3;

    ASSERT_EQ(expected_grille_valeur.cols(), grille_valeur.cols());
    ASSERT_EQ(expected_grille_valeur.rows(), grille_valeur.rows());
    EXPECT_EQ(expected_grille_valeur, grille_valeur) << "grille de bellman des valeurs:\n" << grille_valeur << '\n';

    ASSERT_EQ(expected_grille_parent.cols(), grille_parent.cols());
    ASSERT_EQ(expected_grille_parent.rows(), grille_parent.rows());
    EXPECT_EQ(expected_grille_parent, grille_parent) << "grille de bellman des parents:\n" << grille_parent << '\n';
}

TEST(test_barrage, simulation2) {
    int v_max =2;
    Barrage b(0, v_max, 1, 2, 0);
    vector<float> prix = {1, 2};
    vector<int> ruissellement = {2, 1};
    b.setPrixMarcheElec(prix).setRuissellement(ruissellement);

    b.simuler();
    auto& grille_valeur = b.getGrilleBellmanValeur();
    auto& grille_parent = b.getGrilleBellmanParent();

    Barrage::GrilleBellmanValue expected_grille_valeur(v_max+1, prix.size()+1);
    expected_grille_valeur <<            0.0f,  2.0f, 5.0f,
                                        -1.0f,  1.0f, 5.0f,
                                        -1.0f,  0.0f, 4.0f;
    Barrage::GrilleBellmanParent expected_grille_parent(v_max+1, prix.size()+1);
    expected_grille_parent <<           -1,   0,    1,
                                        -1,   0,    2,
                                        -1,   0,    2;

    ASSERT_EQ(expected_grille_valeur.cols(), grille_valeur.cols());
    ASSERT_EQ(expected_grille_valeur.rows(), grille_valeur.rows());
    EXPECT_EQ(expected_grille_valeur, grille_valeur) << "grille de bellman des valeurs:\n" << grille_valeur << '\n';

    ASSERT_EQ(expected_grille_parent.cols(), grille_parent.cols());
    ASSERT_EQ(expected_grille_parent.rows(), grille_parent.rows());
    EXPECT_EQ(expected_grille_parent, grille_parent) << "grille de bellman des parents:\n" << grille_parent << '\n';
}

TEST(test_barrage, optimisation1) {
    Barrage b(2, 3, 2, 1, 0);
    vector<float> prix = {2, 3, 1};
    vector<int> ruissellement = {1, 2, 0};
    b.setPrixMarcheElec(prix).setRuissellement(ruissellement);

    ASSERT_THROW(b.optimiser(), runtime_error);

    b.simuler();

    vector<int> expected_policy{2, 2, 3, 3};
    EXPECT_EQ(expected_policy, b.optimiser()) << "grille de bellman des valeurs:\n" << b.getGrilleBellmanValeur() << '\n';;
}

TEST(test_barrage, optimisation2) {
    Barrage b(0, 2, 1, 2, 0);
    vector<float> prix = {1, 2};
    vector<int> ruissellement = {2, 1};
    b.setPrixMarcheElec(prix).setRuissellement(ruissellement);
    b.simuler();

    vector<int> expected_policy{0, 2, 1};
    EXPECT_EQ(expected_policy, b.optimiser()) << "grille de bellman des valeurs:\n" << b.getGrilleBellmanValeur() << '\n';;
}
