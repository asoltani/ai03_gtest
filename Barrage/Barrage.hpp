#ifndef AI03_TUTO_BARRAGE_HPP
#define AI03_TUTO_BARRAGE_HPP

#include <vector>
#include <array>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <mutex>
#include <thread>
#include <iostream>
#include <stack>

#include "../eigen/Eigen/Dense"

using namespace std;
using namespace Eigen;


class Barrage {
public:
    typedef Matrix<float, Dynamic, Dynamic> GrilleBellmanValue;
    typedef Matrix<int, Dynamic, Dynamic> GrilleBellmanParent;

private:
    // Les volumes sont exprimés en unité d'eau, par exemple une unité peut être 200m**3. L'unité va déterminer la
    // précision de l'optimisation.
    uint volume_eau; // m**3
    uint volume_eau_max; // m**3
    float rendement; // MWh/m**3
    uint debit_max; // m**3/h
    uint debit_min; // m**3/h

    // ruissellement représente le volume d'eau entrant dans le barrage à chaque pas de temps
    vector<int>* ruissellement = nullptr; // m**3
    // simplification: prix de marché == prix de vente
    vector<float>* prix_marche_elec = nullptr; // €/MWh

    // Grilles utilisées dans Ford-Bellman
    GrilleBellmanValue * grille_bellman_valeur = nullptr;
    GrilleBellmanParent * grille_bellman_parent = nullptr;
    void initialiserGrilleBellmanValeurs();
    void initialiserGrilleBellmanParents();

public:
    Barrage(uint volumeEau, uint volumeEauMax, float rendement, uint debitMax, uint debitMin);
    ~Barrage();

    Barrage& setRuissellement(vector<int>& r);
    Barrage& setPrixMarcheElec(vector<float>& p);

    GrilleBellmanValue & getGrilleBellmanValeur();
    GrilleBellmanParent & getGrilleBellmanParent();

    void simuler();
    vector<int> optimiser();
};


#endif //AI03_TUTO_BARRAGE_HPP
