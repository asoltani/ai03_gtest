#include "Barrage.hpp"


Barrage::Barrage(uint volumeEau, uint volumeEauMax, float rendement, uint debitMax, uint debitMin) :
    volume_eau(volumeEau), volume_eau_max(volumeEauMax), rendement(rendement), debit_max(debitMax), debit_min(debitMin)
{
    if (rendement < 0)
        throw invalid_argument("Le rendement ne peut pas être négatif.");
    if (debit_max < debit_min)
        throw invalid_argument("Le débit max doit être supéreur au débit min.");
    if (volume_eau > volume_eau_max)
        throw invalid_argument("Le volume d'eau doit être inférieur au volume d'eau max.");
}

Barrage::~Barrage() {
    delete grille_bellman_valeur;
    delete grille_bellman_parent;
    delete ruissellement;
    delete prix_marche_elec;
}

Barrage& Barrage::setRuissellement(vector<int>& r) {
    if (prix_marche_elec && r.size() != prix_marche_elec->size())
        throw invalid_argument("La dimension temps de correspond pas");

    ruissellement = new vector<int>;
    for (auto& v : r)
        ruissellement->push_back(v);

    return *this;
}

Barrage &Barrage::setPrixMarcheElec(vector<float> &p) {
    if (ruissellement && p.size() != ruissellement->size())
        throw invalid_argument("La dimension temps de correspond pas");

    prix_marche_elec = new vector<float>;
    for (auto& v : p)
        prix_marche_elec->push_back(v);

    return *this;
}

void Barrage::initialiserGrilleBellmanValeurs() {
    if (!ruissellement && !prix_marche_elec)
        throw runtime_error("Aucun moyen de déterminer la dimension temps.");

    size_t nb_pdt = ruissellement ? ruissellement->size() : prix_marche_elec->size();

    grille_bellman_valeur = new Barrage::GrilleBellmanValue(volume_eau_max+1, nb_pdt+1);
    *grille_bellman_valeur = Barrage::GrilleBellmanValue::Constant(volume_eau_max+1, nb_pdt+1, -1.0f);
}

void Barrage::initialiserGrilleBellmanParents() {
    if (!ruissellement && !prix_marche_elec)
        throw runtime_error("Aucun moyen de déterminer la dimension temps.");

    size_t nb_pdt = ruissellement ? ruissellement->size() : prix_marche_elec->size();

    grille_bellman_parent = new Barrage::GrilleBellmanParent(volume_eau_max+1, nb_pdt+1);
    *grille_bellman_parent = Barrage::GrilleBellmanParent::Constant(volume_eau_max+1, nb_pdt+1, -1);
}

Barrage::GrilleBellmanParent& Barrage::getGrilleBellmanParent() {
    if (grille_bellman_parent)
        return *grille_bellman_parent;
    initialiserGrilleBellmanParents();
    return *grille_bellman_parent;
}

Barrage::GrilleBellmanValue& Barrage::getGrilleBellmanValeur() {
    if (grille_bellman_valeur)
        return *grille_bellman_valeur;
    initialiserGrilleBellmanValeurs();
    return *grille_bellman_valeur;
}

void Barrage::simuler() {
    auto &valeurs = getGrilleBellmanValeur();
    auto &parents = getGrilleBellmanParent();

    valeurs(volume_eau, 0) = 0.0f;

    // Pour chaque pas de temps
    for (size_t pdt = 0; pdt < valeurs.cols(); pdt++)
    {
        // Pour chaque état possible du barrage à ce pas de temps
        for (int volume = 0; volume < valeurs.rows(); volume++)
        {
            if (valeurs(volume, pdt) != -1) {
                // Si l'état est atteignable alors on considère toutes les actions possibles à partir de cette état
                for (int turbinee = debit_min; turbinee <= debit_max; turbinee++)
                {
                    // volume(t+1) = volume(t) - volume_turbinée(t) + ruissellement(t)
                    // Si le volume est supérieur à la capacité du barrage alors il y a débordement et le volume redevient égale à la capacité du barrage
                    int nouveau_volume = min(volume - turbinee + (*ruissellement)[pdt], static_cast<int>(valeurs.rows()) - 1);

                    // Impossible d'atteindre un volume d'eau négatif donc on ne considère pas ces cas là.
                    if (nouveau_volume >= 0)
                    {
                        // valeur(t+1) = valeur(t) + prix(t) * rendement * volume_turbinée(t)
                        float nouvelle_valeur = valeurs(volume, pdt)
                                                + (*prix_marche_elec)[pdt] * rendement * static_cast<float>(turbinee);

                        if (pdt + 1 < valeurs.cols() && nouvelle_valeur > valeurs(nouveau_volume, pdt + 1))
                        {
                            valeurs(nouveau_volume, pdt + 1) = nouvelle_valeur;
                            parents(nouveau_volume, pdt + 1) = volume;
                        }
                    }
                }
            }
        }
    }

    // valorisation du stock de fin
    // On valorise le stock d'eau du barrage en fin de simulation en fonction de la moyenne des prix
    int benefice_moyen = static_cast<int>(accumulate(prix_marche_elec->begin(), prix_marche_elec->end(), 0.0) / prix_marche_elec->size() * rendement);
    for (int volume_final = 0; volume_final < static_cast<int>(valeurs.rows()); volume_final++) {
        valeurs(volume_final, valeurs.cols() - 1) = max(0.0f, valeurs(volume_final, valeurs.cols() - 1)) + static_cast<float>(benefice_moyen * volume_final);
    }
}

vector<int> Barrage::optimiser() {
    auto& valeurs = getGrilleBellmanValeur();
    auto& parents = getGrilleBellmanParent();

    // Recherche du meilleur future possible
    int v_max = -1;
    float b_max = -1.0f;
    for (int v=static_cast<int>(valeurs.rows())-1; v>=0 ; v--) {
        if (valeurs(v, valeurs.cols()-1) > b_max) {
            v_max = v;
            b_max = valeurs(v, valeurs.cols()-1);
        }
    }
    if (v_max == -1)
        throw runtime_error("optimisation impossible");

    // Recherche de la meilleure politique de gestion du barrage pour atteindre ce futur
    int volume = v_max;
    vector<int> meilleure_politique(valeurs.cols());
    for (int pdt= static_cast<int>(meilleure_politique.size()-1); pdt > 0; pdt--)
    {
        meilleure_politique[pdt] = volume;
        volume = parents(volume, pdt);
    }
    meilleure_politique[0] = volume_eau;

    return meilleure_politique;
}